import React, { Fragment, useState } from 'react'
import ReactDOM from 'react-dom'

type FormElem = React.FormEvent<HTMLFormElement>

interface ITodo {
	text: string
	complete: boolean
}

interface ITodo2 extends ITodo {
	tags: string[]
}

export default function App(): JSX.Element {
	const [value, setValue] = useState<string>('')
	const [todos, setTodos] = useState<ITodo[]>([])

	const handleSubmit = (e: FormElem): void => {
		e.preventDefault()

		addTodo(value)

		setValue('')
	}

	const addTodo = (text: string): void => {
		const newTodos: ITodo[] = [
			...todos,
			{ text, complete: false }
		]

		setTodos(newTodos)
	}

	const completeTodo = (idx: number): void => {
		const newTodos: ITodo[] = [...todos]
		newTodos[idx].complete = !newTodos[idx].complete
		setTodos(newTodos)
	}

	const removeTodo = (idx: number): void => {
		let newTodos: ITodo[] = [...todos]
		newTodos.splice(idx, 1)
		setTodos(newTodos)
	}

	console.log(todos)

	return (
		<Fragment>
			<h1>
				Todos
			</h1>
			<form onSubmit={handleSubmit}>
				<input type="text" value={value} onChange={e => setValue(e.target.value)} required />
				<button type="submit">Add Todo</button>
			</form>
			<section>
				{todos.map((todo: ITodo, index: number) => {
					return (
						<Fragment key={index}>
							<div style={{ textDecoration: todo.complete ? 'line-through' : '' }}>{todo.text}</div>
							<button onClick={() => completeTodo(index)}>{todo.complete ? "completed" : "incomplete"}</button>
							<button onClick={() => removeTodo(index)}>Delete</button>
						</Fragment>
					)
				})}
			</section>
		</Fragment>
	)
}

const root = document.getElementById('app-root')

ReactDOM.render(<App />, root)